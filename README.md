# take-out

#### 介绍

外卖系统-springBoot+mybatis_Puls

在已完成的基础功能上加以实现功能

1. 后端

   1. 后台
      1. 菜品、套餐的批量停售、启售、删除功能，并判断商品状态是否可以执行改操作
      2. 订单明细的完善，查询、订单状态、日期查询
   2. 前台 
      1. 实现个人中心、显示最新订单、显示当前用户名、“再来一单”功能实现
      2. 订单中的菜品信息显示、计数商品功能、
      3. 地址功能的完善，判断当前用户有无地址，那么首次添加地址即是默认，反之

2. 前端

   1. 实现个人中心内显示当前用户名(非固定用户名)

   2. 前台登录界面，实现验证码倒计时功能

      

> 验证码倒计时功能参考`shangguan-dragon/reggie-takeout`功能实现

#### 软件架构

外卖系统分为系统管理后台和移动端Web两部分，其中系统管理后台主要提供给餐饮企业内部员工使用，可以对餐厅的分类、菜品、套餐、订单、员工等进行管理维护。移动端应用主要提供给消费者使用，可以在线浏览菜品、添加购物车、下单等。

软件架构

- 前端:H5 Vue.js ElementUI
- 数据库:Mysql、Redis
- 代理服务器 Nginx
- 后端:Spring boot、Mybatis_Puls、Swagge、SpringCache、
- 工具:



#### 使用说明

1.  如果使用单数据库需要更改配置
    1.  注释application.yml数据源的配置
    2.  注释pom.xml文件中的shardingsphere坐标

2.  如果不使用Redis作为缓存
    1.  注释application.yml中redis的相关配置

3.  修改application.yml中datasource的配置信息
4.  图片路径为当前项目内`/src/main/resources/uploadImages/`

