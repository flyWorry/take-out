package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import com.itheima.reggie.service.SetmealDishService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author MSI-PC
 * @create 2022-05-12 23:30
 */
@RestController
@RequestMapping("/dish")
@Slf4j
@Api(tags = "菜品相关接口")
public class DishController {
    @Autowired
    private DishService dishService;
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SetmealDishService setmealDishService;

    /**
     * 新增菜品
     *
     * @param dishDto
     * @return
     */
    @PostMapping
    @CacheEvict(value = {"dishCache","dishPage"},allEntries = true)
    public R<String> save(@RequestBody DishDto dishDto) {
        log.info(dishDto.toString());

        dishService.saveWithFlavor(dishDto);

        //清理所有菜品的缓存数据
        //Set keys = redisTemplate.keys("dish_*");
        //redisTemplate.delete(keys);

//            //清理某个分类下面的菜品缓存数据
//            String key = "dish_" + dishDto.getId() + "_" + "*";
//            redisTemplate.delete(key);

        return R.success("新增菜品成功");
    }

    /**
     * 菜品信息分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @Cacheable(value = "dishPage",key = "#page + '_' + #pageSize+'_'+#name")
    public R<Page> page(int page, int pageSize, String name) {

        Page<Dish> pageInfo = new Page<>(page, pageSize);
        System.out.println("************************************");
        Page<DishDto> dishDtoPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null, Dish::getName, name);
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        dishService.page(pageInfo, queryWrapper);

        BeanUtils.copyProperties(pageInfo, dishDtoPage, "records");
        List<Dish> records = pageInfo.getRecords();

        List<DishDto> list = new ArrayList<>();
        list = records.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);

            //分类ID
            Long categoryId = item.getCategoryId();
            //根据ID查询分类对象名称
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            return dishDto;
        }).collect(Collectors.toList());
//       records.forEach((item)->{
//           DishDto dishDto = new DishDto();
//           BeanUtils.copyProperties(item,dishDto);
//           //分类ID
//           Long categoryId = item.getCategoryId();
//           //根据ID查询分类对象名称
//           Category category = categoryService.getById(categoryId);
//           String categoryName = category.getName();
//           dishDto.setCategoryName(categoryName);
//           list.add(dishDto);
//       });
        dishDtoPage.setRecords(list);
        return R.success(dishDtoPage);
    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<DishDto> get(@PathVariable Long id) {
        DishDto dishDto = dishService.getByIdWithFlavor(id);

        return R.success(dishDto);
    }

    /**
     * 修改菜品
     */
    @PutMapping
    @CacheEvict(value = {"dishCache","dishPage"},allEntries = true)
    public R<String> update(@RequestBody DishDto dishDto) {
        log.info(dishDto.toString());

        dishService.updateWithFlavor(dishDto);

        //清理所有菜品的缓存数据
        //Set keys = redisTemplate.keys("dish_*");
        //redisTemplate.delete(keys);

//            //清理某个分类下面的菜品缓存数据
//            String key = "dish_" + dishDto.getId() + "_" + "*";
//            redisTemplate.delete(key);

        return R.success("修改菜品成功");
    }

    /**
     * 根据条件查询对应的菜品数据
     *
     * @param dish
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "dishCache",key = "#dish.getCategoryId() + '_' + #dish.status")
    public R<List<DishDto>> list(Dish dish) {
        List<DishDto> dishDtos = null;

//            //Redis缓存Key赋值
//            String key = "dish_" + dish.getCategoryId() + "_" + dish.getStatus();//dish_1397844391040167938_1
//            dishDtos = (List<DishDto>) redisTemplate.opsForValue().get(key);
//            if (dishDtos != null) {
//                return R.success(dishDtos);
//            }
//            //如果不存在，需要查询数据库，

        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<Dish>();
        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        queryWrapper.eq(Dish::getStatus, 1);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        List<Dish> list = dishService.list(queryWrapper);

        dishDtos = list.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);

            //分类ID
            Long categoryId = item.getCategoryId();
            //根据ID查询分类对象名称
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }

            Long itemId = item.getId();
            LambdaQueryWrapper<DishFlavor> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(DishFlavor::getDishId, itemId);
            List<DishFlavor> dishFlavors = dishFlavorService.list(wrapper);
            dishDto.setFlavors(dishFlavors);

            return dishDto;
        }).collect(Collectors.toList());
    //        //将查询到的菜品数据缓存到Redis
    //        redisTemplate.opsForValue().set(key, dishDtos, 60, TimeUnit.MINUTES);

        return R.success(dishDtos);
    }

    /**
     * 修改售卖状态
     * @param status
     * @param ids
     * @return
     */
    //http://localhost:8080/dish/status/0?ids=1413384757047271425
    @PostMapping("/status/{status}")
    @CacheEvict(value = {"dishCache","dishPage"},allEntries = true)
    @Transactional
    public R<String> updateDishStatus(@PathVariable Integer status, @RequestParam List<Long> ids) {
        log.info("修改商品售卖状态:" + ids);

        //查询该菜品是否存在已有的套餐内
            LambdaQueryWrapper<SetmealDish> list1 = new LambdaQueryWrapper<>();
            list1.in(null!=ids,SetmealDish::getDishId,ids);
            int count = setmealDishService.count(list1);
            List<String> names =
                    setmealDishService.list(list1).stream().map(SetmealDish::getName).collect(Collectors.toList());
            if (count != 0) {
                throw new CustomException("要停售的菜品集合中存在包含于套餐的菜品 " + names + " ，请检查后再进行删除");
            }
//            ids.forEach(dishId->{
//               queryWrapper1.eq(SetmealDish::getDishId,dishId);
//               List<SetmealDish> list = setmealDishService.list(queryWrapper1);
//               if(list.size()!=0){
//                   throw new CustomException("包含在套餐中的菜品，修改售卖状态失败");
//               }
//            });


        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(null != ids, Dish::getId, ids);

        List<Dish> list = dishService.list(queryWrapper);
//            list.forEach(dish -> {
//                dish.setStatus(status);
//                dishService.updateById(dish);
//            });
        Dish dish = new Dish();
        dish.setStatus(status);
        dishService.update(dish,queryWrapper);
        return R.success("菜品状态已经更改成功！");

    }

    /**
     * 删除商品
     * @param ids
     * @return
     */
    //http://localhost:8080/dish?ids=1413384757047271425
    @DeleteMapping()
    @CacheEvict(value = {"dishCache","dishPage"},allEntries = true)
    @Transactional
    public R<String> deleteDishStatus(@RequestParam List<Long> ids) {
        log.info("删除商品:" + ids);

        //条件构造器：售卖状态为0的
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Dish::getId,ids);
        queryWrapper.eq(Dish::getStatus, 0);
        //获得售卖状态为0的菜品
        List<Dish> list = dishService.list(queryWrapper);
        //判断菜品售卖状态是否都为0
        if (ids.size() != list.size()) {
            throw new CustomException("包含”未停售”的菜品，不能删除");
        }
//        dishService.remove(queryWrapper);
        LambdaUpdateWrapper<Dish> updateWrapper = new LambdaUpdateWrapper<>();
        // set()为sql中的set,in()为sql中的in
        updateWrapper.in(Dish::getId, ids).set(Dish::getIsDeleted, 1);
        dishService.update(updateWrapper);
        //删除菜品关系表中的数据
//        LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
//        queryWrapper1.in(DishFlavor::getDishId, ids);
//        dishFlavorService.remove(queryWrapper1);
        LambdaUpdateWrapper<DishFlavor> updateWrapper1 = new LambdaUpdateWrapper<>();
        updateWrapper1.in(DishFlavor::getDishId, ids).set(DishFlavor::getIsDeleted, 1);
        dishFlavorService.update(updateWrapper1);

        return R.success("菜品删除成功！");
    }


}
