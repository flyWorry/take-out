package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.OrdersDTO;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
@Api(tags = "订单相关接口")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private DishService dishService;
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private DishFlavorService dishFlavorService;

    /**
     * 用户下单
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {
        log.info("订单数据：{}", orders);
        orderService.submit(orders);
        return R.success("下单成功");
    }


    /**
     * 后台订单分页查询
     *
     * @param page
     * @param pageSize
     * @param number
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "后台订单分页查询接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true),
            @ApiImplicitParam(name = "number", value = "订单号", required = false)
    })
//order/page?page=1&pageSize=10&beginTime=2022-05-25%2000%3A00%3A00&endTime=2022-06-29%2023%3A59%3A59
    public R<Page> empPage(int page, int pageSize, String number, String beginTime, String endTime) {
        //分页构造器对象
        Page<Orders> pageInfo = new Page<>(page, pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件，根据name进行like模糊查询
        queryWrapper.like(number != null, Orders::getNumber, number);
        //添加时间条件
        queryWrapper.gt(StringUtils.isNotEmpty(beginTime), Orders::getOrderTime, beginTime);
        queryWrapper.lt(StringUtils.isNotEmpty(endTime), Orders::getOrderTime, endTime);
        //添加排序条件，根据更新时间降序排列
        queryWrapper.orderByDesc(Orders::getOrderTime);

        orderService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

    /**
     * 前台订单查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    @ApiOperation(value = "前台订单分页查询接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true),
    })
    public R<Page> userPage(int page, int pageSize) {
        //获取当前用户ID
        Long userID = BaseContext.getCurrentId();

        //分页构造器对象
        Page<Orders> ordersPage = new Page<>(page, pageSize);

        //查询用户的订单
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(userID != 0, Orders::getUserId, userID);
        queryWrapper.orderByDesc(Orders::getOrderTime);
        orderService.page(ordersPage, queryWrapper);

        //创建空DTO用来承接dtoPage的分页数据
        Page<OrdersDTO> dtoPage = new Page<>();
        //对象拷贝，除了PageInfo内Orders的数据
        BeanUtils.copyProperties(ordersPage, dtoPage, "records");

        //orderPage内的每一条records进行处理
        List<OrdersDTO> ordersDTOList = ordersPage.getRecords().stream().map(orders -> {
            //把orders数据拷贝(类似赋值)给DTO
            OrdersDTO ordersDTO = new OrdersDTO();
            BeanUtils.copyProperties(orders, ordersDTO);

            //查询当前订单的关系表
            LambdaQueryWrapper<OrderDetail> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.in(OrderDetail::getOrderId, orders.getId());
            List<OrderDetail> list = orderDetailService.list(queryWrapper1);

            //DTO内orderDetails赋值
            ordersDTO.setOrderDetails(list);

            return ordersDTO;
        }).collect(Collectors.toList());

        //dtoPage内的records赋值OrdersDTO集合
        dtoPage.setRecords(ordersDTOList);

        return R.success(dtoPage);
    }

    /**
     * 更改订单状态
     *
     * @param orders
     * @return
     */
    //http://localhost:8080/order
    @PutMapping
    public R<Orders> updataStatus(@RequestBody Orders orders) {
        if (orders.getStatus() != 0 && orders.getStatus() != 5) {
            LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(orders.getStatus() != null, Orders::getId, orders.getId());

            orderService.updateById(orders);
            return R.success(orders);
        }

        return R.error("更改订单状态失败");
    }

    /**
     * 订单重购
     * @param orders
     * @return
     */
    //    http://localhost:8080/order/again
    @PostMapping("/again")
    public R again(@RequestBody Orders orders) {
        Long userID = BaseContext.getCurrentId();

        //清空购物车
//        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
//        queryWrapper.eq(ShoppingCart::getUserId,userID);
//        shoppingCartService.remove(queryWrapper);

        //购物车内还有商品
        LambdaQueryWrapper<ShoppingCart> shoppingCartWrapper = new LambdaQueryWrapper<>();
        shoppingCartWrapper.eq(ShoppingCart::getUserId, userID);
        List<ShoppingCart> shoppingCarts = shoppingCartService.list(shoppingCartWrapper);
        shoppingCarts.stream().map(dishOrSetmeal -> {
            if (dishOrSetmeal != null) {
                throw new CustomException("购物车内已有商品，请清空购物车后尝试操作");
            }
            return null;
        }).collect(Collectors.toList());

        //根据订单ID查询关系表
        LambdaQueryWrapper<OrderDetail> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(OrderDetail::getOrderId, orders.getId());
        List<OrderDetail> list = orderDetailService.list(queryWrapper);


        List<ShoppingCart> collect = list.stream().map((item) -> {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setUserId(userID);
            //加入购物车
//            if (item.getDishId() != null) {
//                LambdaQueryWrapper<Dish> queryWrapper1 = new LambdaQueryWrapper<>();
//                queryWrapper1.in(Dish::getId, item.getDishId());
//                Dish dish = dishService.getOne(queryWrapper1);
//                shoppingCart.setName(dish.getName());
//                shoppingCart.setImage(dish.getImage());
//                shoppingCart.setDishId(dish.getId());
//                shoppingCart.setDishFlavor(item.getDishFlavor());
//                shoppingCart.setNumber(item.getNumber());
//                shoppingCart.setAmount(BigDecimal.valueOf(dish.getPrice().intValue()));
//            } else if (item.getSetmealId() != null) {
//                LambdaQueryWrapper<Setmeal> queryWrapper1 = new LambdaQueryWrapper<>();
//                queryWrapper1.in(Setmeal::getId, item.getSetmealId());
//                Setmeal one = setmealService.getOne(queryWrapper1);
//                shoppingCart.setName(one.getName());
//                shoppingCart.setImage(one.getImage());
//                shoppingCart.setDishId(one.getId());
//                shoppingCart.setDishFlavor(item.getDishFlavor());
//                shoppingCart.setNumber(item.getNumber());
//                shoppingCart.setAmount(one.getPrice());
//            }
            shoppingCart.setName(item.getName());
            shoppingCart.setImage(item.getImage());
            shoppingCart.setUserId(BaseContext.getCurrentId());
            shoppingCart.setDishId(item.getDishId());
            shoppingCart.setSetmealId(item.getSetmealId());
            shoppingCart.setDishFlavor(item.getDishFlavor());
            shoppingCart.setNumber(item.getNumber());
            shoppingCart.setAmount(item.getAmount());
            return shoppingCart;
        }).collect(Collectors.toList());
        shoppingCartService.saveBatch(collect);

        return R.success("再来一单啊");
    }


}