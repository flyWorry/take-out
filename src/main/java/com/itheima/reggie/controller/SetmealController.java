package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author MSI-PC
 * @create 2022-05-13 23:40
 */

@RestController
@RequestMapping("/setmeal")
@Slf4j
@Api(tags = "套餐相关接口")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SetmealDishService setmealDishService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @PostMapping
    @CacheEvict(value = {"setmealCache","setmealPage"}, allEntries = true)
    @ApiOperation(value = "新增套餐接口")
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        log.info("套餐信息：{}", setmealDto);

        setmealService.saveWithDish(setmealDto);

        return R.success("新增套餐成功");
    }

    /**
     * 套餐分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "套餐分页查询接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true),
            @ApiImplicitParam(name = "name", value = "套餐名称", required = false)
    })
    @Cacheable(value = "setmealPage",key = "#page + '_' + #pageSize+'_'+#name")
    public R<Page> page(int page, int pageSize, String name) {
        //分页构造器对象
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> dtoPage = new Page<>();

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件，根据name进行like模糊查询
        queryWrapper.like(name != null, Setmeal::getName, name);
        //添加排序条件，根据更新时间降序排列
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        setmealService.page(pageInfo, queryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        List<Setmeal> records = pageInfo.getRecords();

        List<SetmealDto> list = records.stream().map((item) -> {
            SetmealDto setmealDto = new SetmealDto();
            //对象拷贝
            BeanUtils.copyProperties(item, setmealDto);
            //分类id
            Long categoryId = item.getCategoryId();
            //根据分类id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                //分类名称
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(list);
        return R.success(dtoPage);
    }

//    /**
//     * 删除套餐
//     *
//     * @param ids
//     * @return
//     */
//    @DeleteMapping
//    @CacheEvict(value = "setmealCache", allEntries = true)
//    @ApiOperation(value = "套餐删除接口")
//    public R<String> delete(@RequestParam List<Long> ids) {
//        log.info("ids:{}", ids);
//
//        setmealService.removeWithDish(ids);
//
//        return R.success("套餐数据删除成功");
//    }

    /**
     * 前端页面-根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    @Cacheable(value = "setmealCache", key = "#setmeal.categoryId + '_' + #setmeal.status")
    @ApiOperation(value = "套餐条件查询接口")
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> list = setmealService.list(queryWrapper);

        return R.success(list);
    }

    /**
     * 根据id查询套餐信息和对应的菜品信息
     *
     * @param id
     * @return
     */
    //http://localhost:8080/setmeal/1531297802468802561
    @GetMapping("/{id}")
    @ApiOperation(value = "根据ID获取套餐和菜品信息接口")
    public R<SetmealDto> getSetmeal(@PathVariable Long id) {
        SetmealDto setmealDto = setmealService.getByIdWith(id);

        return R.success(setmealDto);
    }


    /**
     * 修改套餐
     *
     * @param setmealDto
     * @return
     */
    //http://localhost:8080/setmeal
    @PutMapping
    @ApiOperation(value = "修改套餐接口")
    @CacheEvict(value = {"setmealCache","setmealPage"}, allEntries = true)
    @Transactional
    public R<String> updateSetmeal(@RequestBody SetmealDto setmealDto) {

        setmealService.updateSetmeal(setmealDto);

        return R.success("修改套餐成功");
    }
    /**
     * 修改售卖状态
     * @param status
     * @param ids
     * @return
     */
    //http://localhost:8080/setmeal/status/0?ids=1531297802468802561
    @PostMapping("/status/{status}")
    @ApiOperation(value = "修改套餐售卖状态接口")
    @CacheEvict(value = {"setmealCache","setmealPage"}, allEntries = true)
    @Transactional
    public R<String> updateDishStatus(@PathVariable Integer status, @RequestParam List<Long> ids) {
        log.info("修改套餐售卖状态:" + ids);


        LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(ids.size()!=0,Setmeal::getId, ids);
        updateWrapper.set(Setmeal::getStatus, status);
        setmealService.update(updateWrapper);

        return R.success(status == 0 ? "已停售" : "已启售");

    }

    /**
     * 删除套餐
     * @param ids
     * @return
     */
    //http://localhost:8080/setmeal?ids=1531297802468802561
    @DeleteMapping
    @ApiOperation(value = "删除套餐接口")
    @CacheEvict(value = {"setmealCache","setmealPage"}, allEntries = true)
    @Transactional
    public R<String> deleteSetmeal(@RequestParam List<Long> ids) {
        log.info("删除商品:" + ids);
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();

        //条件构造器：套餐内售卖状态为0的
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus, 0);
        //获得
        List<Setmeal> list = setmealService.list(queryWrapper);
        if (ids.size() != list.size()) {
            throw new CustomException("包含正在售卖中的套餐，不能删除");
        }
//        setmealService.remove(queryWrapper);
        LambdaUpdateWrapper<Setmeal> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(Setmeal::getId, ids).set(Setmeal::getIsDeleted, 1);
        setmealService.update(updateWrapper);

        //删除套餐关系表中的数据
//        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
//        queryWrapper1.in(SetmealDish::getSetmealId, ids);
//        setmealDishService.remove(queryWrapper1);
        LambdaUpdateWrapper<SetmealDish> updateWrapper1 = new LambdaUpdateWrapper<>();
        updateWrapper1.in(SetmealDish::getSetmealId, ids).set(SetmealDish::getIsDeleted, 1);
        setmealDishService.update(updateWrapper1);

        return R.success("套餐删除成功！");
    }


}
