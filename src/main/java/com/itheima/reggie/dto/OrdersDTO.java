package com.itheima.reggie.dto;

import com.itheima.reggie.entity.OrderDetail;
import com.itheima.reggie.entity.Orders;
import lombok.Data;

import java.util.List;

/**
 * @author MSI-PC
 * @create 2022-06-03 22:12
 */
@Data
public class OrdersDTO extends Orders {

    private List<OrderDetail> orderDetails;

}
