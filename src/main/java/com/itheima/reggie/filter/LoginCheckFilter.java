package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author MSI-PC
 * @create 2022-05-09 22:07
 */

@WebFilter(filterName = "loginCheckFilter",urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    //路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //1 获取uri
        String requestURL = request.getRequestURI();
        log.info("拦截到请求：{}",requestURL);
        //放行路径
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",  // 移动端 发送短信
                "/user/login" ,    //  移动端登录
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };
        //2 判断请求处理
        boolean check = check(urls, requestURL);
        //3处理
        if(check){
            log.info("请求可放行：{}",requestURL);
            filterChain.doFilter(request,response);
            return;
        }
        //4-1、PC判断登录状态，如果已登录，则直接放行
        if(request.getSession().getAttribute("employee") != null){
            // 自定义元数据对象处理器 MyMetaObjectHandler中需要使用 登录用户id
            log.info("后台用户已登录，用户id：{}",request.getSession().getAttribute("employee"));

            LoginPass("employee",request,response,filterChain);
            return;
        }
        //4-2、MT判断登录状态，如果已登录，则直接放行
        if(request.getSession().getAttribute("user") != null ){
            // 自定义元数据对象处理器 MyMetaObjectHandler中需要使用 登录用户id
            log.info("前台用户已登录，用户id：{}",request.getSession().getAttribute("user"));

            LoginPass("user",request,response,filterChain);
            return;
        }
        //5 没登录，返回未登录的结果
        // 通过输出流 向客户端页面响应数据
        // 返回结果需要是 backend/js/request.js 中写好的返回结果
        log.info("当前的用户未登录：{}",requestURL);
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;

    }

    /**
     * 路径匹配，检查本次请求是否需要放行
     * @param requestURI
     * @return
     */
    public boolean check(String[] urls,String requestURI){
        for(String url:urls){
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) {
                return true;
            }
        }
        return false;

    }

    /**
     * 登录放行
     * @param name
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    public void LoginPass(String name, HttpServletRequest request, HttpServletResponse response,
                          FilterChain filterChain) throws ServletException, IOException {
//获取HttpSession中的登录用户信息, 调用BaseContext的setCurrentId方法将当前登录用户ID存入ThreadLocal
        Long empId = (Long) request.getSession().getAttribute(name);
        BaseContext.setCurrentId(empId);

        filterChain.doFilter(request,response);
    }

}
