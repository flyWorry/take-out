package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MSI-PC
 * @create 2022-05-12 23:28
 */
@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}
