package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MSI-PC
 * @create 2022-05-12 15:26
 */

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
