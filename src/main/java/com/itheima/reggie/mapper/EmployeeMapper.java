package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MSI-PC
 * @create 2022-05-09 19:27
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
