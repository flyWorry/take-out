package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MSI-PC
 * @create 2022-05-13 23:37
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {



}
