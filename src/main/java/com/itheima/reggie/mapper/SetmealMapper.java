package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MSI-PC
 * @create 2022-05-12 15:26
 */

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
    public void saveWithDish(SetmealDto setmealDto);
}
