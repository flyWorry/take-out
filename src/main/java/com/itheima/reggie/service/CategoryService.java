package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Category;

/**
 * @author MSI-PC
 * @create 2022-05-09 19:28
 */

public interface CategoryService extends IService<Category> {

    public void remove(Long id);

}
