package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.DishFlavor;

/**
 * @author MSI-PC
 * @create 2022-05-12 23:28
 */

public interface DishFlavorService extends IService<DishFlavor> {
}
