package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;

/**
 * @author MSI-PC
 * @create 2022-05-09 19:28
 */

public interface EmployeeService extends IService<Employee> {
}
